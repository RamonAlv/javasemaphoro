
public class Clientes{
    private int Mesa, Orden;
    public Clientes(int Mesa, int Orden){
        this.Mesa = Mesa;
        this.Orden = Orden;
    }

    public int getMesa(){
        return this.Mesa;
    }

    public int getOrden(){
        return this.Orden;
    }

    public void setOrden(int orden){
        this.Orden = orden;
    }
}